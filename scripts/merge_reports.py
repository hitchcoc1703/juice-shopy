import pandas as pd

semgrep_df = pd.read_csv("semgrep_output.txt", delimiter="\t")
sonarqube_df = pd.read_csv(".scannerwork/report-task.txt", delimiter="\t")
zap_df = pd.read_html("zap_report.html")[0]

merged_df = pd.concat([semgrep_df, sonarqube_df, zap_df], ignore_index=True)
merged_df.to_csv("merged_report.txt", index=False, sep='\t')