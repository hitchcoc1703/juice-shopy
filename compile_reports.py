import xmltodict

def convert_xml_to_dict(file_path):
    with open(file_path, 'r') as file:
        data = file.read()
    return xmltodict.parse(data)

def write_to_txt(file_path, content):
    with open(file_path, 'w') as file:
        file.write(content)

if __name__ == "__main__":
    semgrep_report = open('semgrep_report.txt', 'r').read()
    # Примерный парсинг ZAP отчета
    zap_report = convert_xml_to_dict('zap_report.xml')

    final_output = "=== SEMGREP REPORT ===\n"
    final_output += semgrep_report + "\n\n"
    final_output += "=== ZAP REPORT ===\n"
    final_output += str(zap_report) + "\n"

    write_to_txt('final_report.txt', final_output)