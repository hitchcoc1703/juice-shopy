   FROM node:14

   # Установка зависимостей
   WORKDIR /app
   COPY package*.json ./
   RUN npm install

   # Копирование исходных кодов
   COPY . .

   EXPOSE 3000
   CMD ["npm", "start"]